import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

const urlApi = "http://localhost:8080/api/v1/employees"

function App() {
  const [data, setData] = useState([])

  const getAllData = async () => {
    try {
      const res = await axios.get(urlApi)
      setData(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    emailId: ""
  })

  const createPerson = async (formData: any) => {
    try {
      await axios.post(urlApi, formData)
      alert("Tao thanh cong")
    } catch (e) {
      console.log(e)
      alert("Tao that bai")
    }
  }

  const submitData = (formData: any) => {
    createPerson(formData)
  }

  const [employee, setEmployee] = useState({
    firstName: "",
    lastName: "",
    emailId: "",
    id: 0
  })
  const getDataById = async (id: any) => {
    try {
      const res = await axios.get(`${urlApi}/${id}`)
      setEmployee({
        firstName: res.data.firstName,
        lastName: res.data.lastName,
        emailId: res.data.emailId,
        id: res.data.id
      })
    } catch (e) {
      console.log(e)
    }
  }
  console.log(employee)

  const [isShow, setIsShow] = useState(false)

  const showFormEdit = async (id: any) => {
    await getDataById(id)
    setIsShow(true)
  }


  const removeEmployee = async (id: any) => {
    try {
      await axios.delete(`${urlApi}/${id}`)
      alert("Xoa thanh cong")
    } catch (e) {
      console.log(e)
    }
  }

  const updateData = async (data: any, id: any) => {
    try {
      await axios.put(`${urlApi}/${id}`, data)
      alert("Cap nhat thanh cong")
    } catch (e) {
      console.log(e)
    }
  }

  const deleteEmployee = (id: any) => {
    removeEmployee(id)
  }

  useEffect(() => {
    getAllData()
  }, [])

  const changeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmployee({...employee, [e.target.name]: e.target.value})
  }

  console.log(employee)
  return (
    <div className="App">


      <div className="list-employee">
        <h3>Employee list</h3>
        <div className="list">
          {!!data && data.map((i: any) => <div key={i.id} className="list-item">
            <div>
              <div><p>Email: {i.emailId}</p></div>
              <div><p>First name: {i.firstName}</p></div>
              <div><p>Last name: {i.lastName}</p></div>
              <button onClick={() => showFormEdit(i.id)}>Edit</button>
              <button onClick={() => deleteEmployee(i.id)}>Delete</button>
            </div>
          </div>)}
        </div>
      </div>

      <div className="create-box">
        <div className="input-element"><p>First name</p><input type="text" className='input-field' onChange={(e: any) => setFormData({
          ...formData,
          firstName: e.target.value
        })} /></div>
        <div className="input-element"><p>Last name</p><input type="text" className='input-field' onChange={(e: any) => setFormData({
          ...formData,
          lastName: e.target.value
        })} /></div>
        <div className="input-element"><p>Email</p><input type="text" className='input-field' onChange={(e: any) => setFormData({
          ...formData,
          emailId: e.target.value
        })} /></div>
        <button onClick={() => submitData(formData)}>Create</button>
      </div>


      {isShow && <div className="create-box">
              <h3>Edit</h3>
              <div className="input-element"><p>First name</p><input value={employee?.firstName || ""} name="firstName" type="text" className='input-field' onChange={changeInput} /></div>
              <div className="input-element"><p>Last name</p><input value={employee?.lastName || ""} name="lastName" type="text" className='input-field' onChange={changeInput} /></div>
              <div className="input-element"><p>Email</p><input value={employee?.emailId || ""} name="emailId" type="text" className='input-field' onChange={changeInput} /></div>
              <div>

              </div>
              <button onClick={() => updateData(employee, employee.id)}>Save</button>
            </div>}
    </div>
  );
}

export default App;
